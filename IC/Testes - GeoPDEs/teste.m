n = 50
m=zeros(n, n)
u=@(x, y)(x^2+y^2-3*sqrt(x^2+y^2)+2)*sin(2*atan(y/x))
for i=1:n
  for j=1:n
    m(i, j) = u(2/n * i, 2/n * j)
   end
end

plot(m)