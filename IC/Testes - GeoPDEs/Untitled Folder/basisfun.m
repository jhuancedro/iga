function N = basisfun(P,n, t)
    % Input
    % P=input(’Order of the B-spline’)
    % n=input(’No of control points’)
    %Output
    % b= Symbolic basis function in u.
    % each knot span contains P+1 no of basis functions in cell format.
%     t = knotseq(n, P-1);
%     t = cell2mat({0 0 0 0.333 0.667 1 1 1});
    
    [q,p] = size (t);
    r = P;
    v = (t(1,r)+t(1,r+1))/2;
    c = cell(n+1,P);e=cell(n+1,1);
    syms u;
    a=1;b=1;

    while r<p-(P-1)
        for j = 1:P
            for i = 1:(n+1)
                if j == 1
                    if v >= t(1,i) && v < t(1,i+1)
                        c{i, j} = 1;
                    else
                        c{i,j} = 0;
                    end
                else
                    y = 0;
                    dn1 = u - t(1, i);
                    dd1 = t(1, i+j-1) - t(1,i);
                    if dd1 ~= 0
                        y = c{i, j-1}.*(dn1/dd1);
                    end
                    dn2 = t(1, i+j) - u;
                    dd2 = t(1, i+j) - t(1, i+1);
                    if dd2 ~= 0
                        y = y + c{i+1, j-1}.*(dn2/dd2);
                    end
                    c{i, j} = y;
                end
            end
        end

        for ad = 0:P-1
            e{a+ad, 1} = c{b+ad, P};
        end
        a = a+P;
        b = b+1;
        r = r+1;
        v = (t(1,r) + t(1,r+1))/2;
    end
    e = reshape(e, P, P)'; 