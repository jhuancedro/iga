function t = knotseq(p, n)
    % Input
    % p=input(’Order of the B-spline’)
    % n=input(’No of control points’)
    %Output
    % t= clamped knot sequence
    
    len_t =  n+p+2;
    t = cell(1, len_t);
    
    % preencher extremidades
     for i=1:p-1
        t(i) = {0};
        t(len_t+1-i) = {1};
     end
     
     %preencher meio
     c = num2cell(linspace(0, 1, n-p+4))
     t(p:p+n)
     t = cell2mat(t)
end