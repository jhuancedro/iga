clear
clc
syms u
nel=3%input('Enter the number of elements');
p=1;
k=3;
s=1/(nel+k-2);%difference between two control points
nv=1/(2*nel);

% finding control points
for i=0:s:1
    f(p,1)=i;
    p=p+1;
end

h=nel-1+2*k;
z=h-k;
% finding the knot vector
for i=1:h
    if i<=k
        t(1,i)=0;
    elseif i>z
        t(1,i)=1;
    else
        t(1,i)=(i-k)*2*nv;
    end
end

%finding basis functions
g=cell(k*nel,1);
g=basisfun(p,k, t);

% Calculating global stiffness and global mass matrix
dummy1=zeros(nel+k-1);
dummy2=zeros(nel+k-1);
KT=zeros(nel+k-1);
MT=zeros(nel+k-1);
j=1;
o=1;
q=0;
v=2*nv;
for i=1:nel
    N1(u)=g{o,1};
    N2(u)=g{o+1,1};
    N3(u)=g{o+2,1};
    du1=diff(N1)*f(j,1)+diff(N2)*f(j+1,1)+diff(N3)*f(j+2,1);
    n1=[N1 N2 N3];
    B1=diff(n1)/du1;
    k1=B1'*B1;
    k1=k1*du1;
    Ke1=int(k1,q,q+v);
    Ke1=double(Ke1(u));
    m1=n1'*n1;
    m1=m1*du1;
    Me1=int(m1,q,q+v);
    j=j+1;
    o=o+3;
    q=q+v;
    dummy1([i:i+2],[i:i+2])=Ke1;
    KT=KT+dummy1;
    dummy1=zeros(nel+k-1);
    dummy2([i:i+2],[i:i+2])=Me1(u);
    MT=MT+dummy2;
    dummy2=zeros(nel+k-1);
end

% applying boundary conditions and finding natural frequencies
w1=eig(KT(2:end-1,2:end-1),MT(2:end-1,2:end-1));
nw1=sqrt(w1);

% for plotting the ratio frequencies to element number
for i=1:nel
    na1(i,1)=nw1(i,1)/(i*pi);
    el(i,1)=i/(nel);
end

plot(el,na1,'k')