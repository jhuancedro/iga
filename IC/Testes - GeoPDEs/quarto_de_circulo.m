'''Apendix A'''

coefs(1:3, 1, 1) = [1; 0; 0];                      coefs(4, 1, 1) = 1;
coefs(1:3, 1, 2) = [sqrt(2)/2; sqrt(2)/2; 0];      coefs(4, 1, 2) = sqrt(2)/2;
coefs(1:3, 1, 3) = [0; 1; 0];                      coefs(4, 1, 3) = 1;
coefs(1:3, 2, 1) = [2; 0; 0];                      coefs(4, 2, 1) = 1;
coefs(1:3, 2, 2) = [sqrt(2); sqrt(2); 0];          coefs(4, 2, 2) = sqrt(2)/2;
coefs(1:3, 2, 3) = [0; 2; 0];                      coefs(4, 2, 3) = 1;
knots = {[0 0 1 1], [0 0 0 1 1 1]};
nurbs = nrbmak(coefs, knots);
nrbctrlplot(nurbs)

nurbs = nrbdegelev(nurbs, [1 0]);
new_knots = linspace(0, 1, 10);
nurbs = nrbkntins(nurbs, {new_knots(2:end-1) new_knots(2:end-1)});

nrbctrlplot(nurbs)

% linha = nrbline([1 0 0], [2 0 0]);
% anel = nrbrevolve(linha, [0 0 0], [0 0 1], pi/2);
% nrbctrlplot(anel);