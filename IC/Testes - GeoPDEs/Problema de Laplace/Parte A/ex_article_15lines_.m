'''Modificacoes aleatorias no ex_article_15lines'''

% EX_ARTICLE_15LINES: minimalistic tutorial in 15 lines... Sorry, 16 lines.
%
% Example to solve the problem
%
%    - div ( grad (u)) = (8-9*sqrt(x.^2+y.^2)).*sin(2*atan(y./x))./(x.^2+y.^2)  in Omega
%                    u = 0                                                      on Gamma
%
% with                Omega = (1 < x^2+y^2 < 4) & (x > 0) & (y > 0)
% and exact solution      u = (x.^2+y.^2-3*sqrt(x.^2+y.^2)+2).*sin(2.*atan(y./x))
%
% This solves the example of Section 4 in the article
%

% Carrega os dados de uma geometria anular
geometry = geo_load('ring_refined.mat');  
knots = geometry.nurbs.knots;

% Define a regra da quadratura gaussiana (nos e pesos)
rule = msh_gauss_nodes (geometry.nurbs.order);

% calcula a posicao e pesos dos nos da quadratura em um 'tensor product grid'
[qn, qw] = msh_set_quad_nodes (knots, rule);

% contrutor da classe para malhas cartesianas
msh = msh_cartesian (knots, qn, qw, geometry);

% construtor da classe do tensor-produto de espacos da NURBS
space  = sp_nurbs (geometry.nurbs, msh);

% monta a matriz de rigidez
mat = op_gradu_gradv_tp(space, space, msh);

%f = @(x, y) (8-9*sqrt(x.^2+y.^2)).*sin(2*atan(y./x))./(x.^2+y.^2);
f = @(x, y) sin(x.*1);
%f = @(x, y) x.*0;

% monta o "vetor do lado direito"
rhs = op_f_v_tp (space, msh, f);

drchlt_dofs = [];
for iside = 1:4
  % conjunto dos graus de liberdade na fronteira (no sistema glogal)
  drchlt_dofs = union (drchlt_dofs, space.boundary(iside).dofs);
end

% conjunto dos graus de liberdade que
% nao estao na fronteira (no sistema glogal)
int_dofs = setdiff (1:space.ndof, drchlt_dofs);

u = zeros (space.ndof, 1);

% POR QUE DESSA OPERACAO??:
% u(int_dofs) = inverse(mat(int_dofs, int_dofs)) * rhs(int_dofs);
u(int_dofs) = mat(int_dofs, int_dofs) \ rhs(int_dofs);

sp_to_vtk (u, space, geometry, [20 20], 'laplace_solution2.vts', 'u')
err = sp_l2_error (space, msh, u, f)

%% COMO MUDAR AS CONDICOES DE CONTORNO??