f = @(x, y) (8-9*sqrt(x.^2+y.^2)).*sin(2*atan(y./x))./(x.^2+y.^2);
f_ = @(x, y) ifelse(abs(f(x, y)) < 5, f(x, y), NaN);

f2 = @(x, y) sin(x);
f2_ = @(x, y) ifelse(1 < sqrt(x.^2+y.^2) &
                     sqrt(x.^2+y.^2) < 2 &
                     x > 0 & y > 0,
                     f2(x, y), NaN);

%{
function z = f(x, y)
  if(x.^2 + y.^2 > 0.6.^2)
    z = (8-9*sqrt(x.^2+y.^2)).*sin(2*atan(y./x))./(x.^2+y.^2);
  else
    z = 0;
   end
end
%}

x = y = linspace(-2, 2, 50);
[xx, yy] = meshgrid(x, y);
mesh(x, y, f2_(xx, yy));
%zlim ([-10, 10])
%zlim ([-5, 5])

xlabel("eixo x")
ylabel("eixo y")
zlabel("eixo z")