'''Nao faco ideia de onde tirei este codigo, mas da erro'''


% EX_ADVECTION_DIFFUSION_SQUARE: solve the advection-diffusion problem in the unit square.

% PHYSICAL DATA OF THE PROBLEM
clear problem_data  
% Physical domain, defined as NURBS map given in a text file
problem_data.geo_name = 'dominio.txt';

% Type of boundary conditions for each side of the domain
problem_data.nmnn_sides   = [];
problem_data.drchlt_sides = [1 2 3 4];

% Physical parameters
problem_data.c_diff = @(x , y) ones( size (x ));    % diffusion coefficient (mu)
problem_data.grad_diff = @(x , y) ones ([2 , size(x)]);  % grad(mu)
problem_data.vel = @(x , y) cat( 1 , reshape( zeros( size(x)) , [1 , size(x)]), ...
                                     reshape( zeros( size(x)) , [1 , size(x)]));
problem_data.f = @(x , y) zeros( size (x));
problem_data.g = @(x , y , ind) zeros( size(x));
problem_data.h = @teste_Drchlt_Laplace ;

% CHOICE OF THE DISCRETIZATION PARAMETERS
clear method_data
method_data.degree     = [1 1];    % Degree of the splines
method_data.regularity = [0 0];    % Regularity of the splines
method_data.nsub       = [20 20];  % Number of subdivisions
method_data.nquad      = [1 1];    % Points for the Gaussian quadrature rule
method_data.stab = true; % Choose whether to perform stabilization (true/false)

% CALL TO THE SOLVER
[geometry, msh, space, u] = solve_adv_diff_2d (problem_data, method_data);

% POST-PROCESSING
% Export to Paraview

output_file = 'Advection_Diffusion_square';

vtk_pts = {linspace(0, 1, 20), linspace(0, 1, 20)};
fprintf ('The result is saved in the file %s \n \n', output_file);
sp_to_vtk (u, space, geometry, vtk_pts, output_file, 'u')

% Plot in Matlab

[eu, F] = sp_eval (u, space, geometry, vtk_pts);
[X, Y]  = deal (squeeze(F(1,:,:)), squeeze(F(2,:,:)));
surf (X, Y, eu)

disp ('The overshoots come from using the L2 projection for Dirichlet boundary conditions')
disp ('This can be fixed setting the boundary conditions in a different way (see solve_adv_diff_2d)')


%!demo
%! ex_advection_diffusion_square

%{
clear

problem_data.geo_name = 'dominio.txt';
problem_data.nmnn_sides = [ ];
problem_data.drchlt_sides = [1 2 3 4];
problem_data.c_diff = @(x , y) ones( size (x ));
problem_data.grad_diff = @(x , y) ones ([2 , size(x)]);
problem_data.vel = @(x , y) cat( 1 , reshape( zeros( size(x)) , [1 , size(x)]), ...
                                     reshape( zeros( size(x)) , [1 , size(x)]));
problem_data.f = @(x , y) zeros( size (x));
problem_data.g = @(x , y , ind) zeros( size(x));
problem_data.h = @teste_Drchlt_Laplace ;

method_data.degree = [1 1];
method_data.regularity = [0 0];
method_data.nsub = [20 20];
method_data.nquad = [1 1];
method_data.stab = false;

[geometry, msh, space, u] = solve_laplace (problem_data, method_data);
[ knots , zeta ] = kntrefine(geometry.nurbs.knots , nsub-1, degree, regularity);
%}