## Teste lambdas
# from copy import deepcopy as copy
#
# # lambdas = [(lambda y: lambda x: x+y)(i) for i in range(10)]
# lambdas = []
# for i in range(10):
#     # lambdas.append((lambda y: (lambda x: x+y))(i))
#     lambdas.append(lambda x: x+copy(i))
#
# for f in lambdas:
#     print(f(10), f)

# from NURBS import *
#
# B = BasesNURBS(clamped_uniform_knots(0, 1, 5, 3), 3)
# # B.plotBase()
#
# dB = B.diff()
# # plotFuncoesSeg(dB)
#
# for dBi in dB:
#     dBi.integrate(dBi.knots[-1], dBi.knots[-1]).plot()

from jhc.AuxDisplay import *
import jhc.nurbs as nb
import jhc.iga as iga

import pylab as pl
import sympy as sp

rho, g, E, L = 1, 1, 1, 2


def teste_nurbs(n, p, knots=None):
    # --------------   NURBS   -------------------
    x = sp.Symbol('x')

    if knots is None:
        knots = nb.clamped_uniform_knots(0, 1, n, p)
    displayEq('\Xi', sp.Array(knots))

    B = nb.BasesNURBS(knots, p)
    # B.plotBase()
    # N = B.N[-1]

    return B, x


def teste_iga(B, cp_x, knt_spans, u, x):
    # ----------------   IGA   ---------------------

    problema = iga.Problema(B, p, x)
    problema.construir_matriz_global(cp_x, knt_spans)
    u_h = problema.solve()
    displayEq('u_h', u_h)

    # solução analítica
    pl.figure(1)
    eixo = pl.linspace(0, 2, 500)
    pl.plot(eixo, u(eixo), 'k-', label='Solução analítica')

    pl.figure(2)
    pl.plot(eixo, -eixo + 2, 'k-', label='Tensão axial analítica')

    problema.plotSolucao()
    # problema.plotDiffSolucao()

    return problema


def teste(n, p, cp_x, knt_spans, u, knots=None):
    bases, x = teste_nurbs(n, p)
    return teste_iga(bases, cp_x, knt_spans, u, x)


# x = sp.Symbol('x')
'''
n = 4
p = 1

cp_x = (0.0, 0.75, 1.5, 2.0)
knt_spans = [(0, 2), (2, 3), (3, 5)]

u = lambda x: rho*g / E * (L * x - x**2/2)

# _ = teste(n, p, cp_x, knt_spans, u)
B, x = teste_nurbs(n, p)
teste_iga(B, cp_x, knt_spans, u, x)
'''
'''
B.elevarGrau()
# B.plotBase()

display(B.knots)

cp_x = (0.0, 1.0, 2.0)
knt_spans = [(0, 3), (3, 5)]

teste_iga(B, cp_x, knt_spans, u, x)
'''
# knots = (0, 0, 0, 1, 2, 3, 4, 4, 5, 5, 5)
# displayEq('\Xi', sp.Array(knots))
#
# # Criando base de grau p=5
# B = nb.BasesNURBS(knots)
# f = pl.figure()
# B.plotBase()
# B.elevarGrau()
# B.plotBase()
# B.elevarGrau()
# B.plotBase()

n = 3
p = 1

cp_x = (0.0, 1.0, 2.0)
knt_spans = [(0, 2), (2, 4)]

u = lambda x: rho * g / E * (L * x - x ** 2 / 2)

_ = teste(n, p, cp_x, knt_spans, u)
