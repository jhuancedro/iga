import jhc.nurbs as nb

p = 2
n = 5
knots_x = nb.clamped_uniform_knots(0, 1, 5, 2)
Bx = nb.BasesNURBS(knots_x, p)
Bx.plotBase()