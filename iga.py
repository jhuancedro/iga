import sympy as sp
from AuxDisplay import *
import NURBS as nb
from itertools import product

A, E, rho, g = sp.var('A, E, rho, g')


class Problema:
    def __init__(s, base: nb.BasesNURBS, p, x):
        s.N = base.N[-1]
        s.knots = base.knots
        s.p = p
        s.x = x
        s.K = None
        s.F = None
        s.cp_x = None
        s.u = None

    def matrizes_locais(s, m, knots, cp_x, knt_spans):
        s.cp_x = cp_x
        k = []
        f = []
        for i_ks, ks in enumerate(knt_spans):
            #         print('ks:', ks)
            x_inf, x_sup = knots[ks[0]], knots[ks[1]]
            cp_x_ks = cp_x[i_ks: i_ks + s.p + 1]
            N_ks = s.N[i_ks: i_ks + s.p + 1]

            # for i, n in enumerate(N_ks):
            #     n.plot('N %d'%i)
            # nb.pl.xlim(x_inf, x_sup)
            # nb.plotShow()
            # print('cp_x_ks:', cp_x_ks)
            print('knot_span[%f, %f]'%(x_inf, x_sup))

            J = sum(N_ks[i].diff(s.x) * cp_x_ks[i] for i in range(m))
            # J.display('J')

            k.append(sp.zeros(3))
            f.append(sp.zeros(3, 1))
            for i, j in product(range(m), repeat=2):
                AUX = N_ks[i].diff(s.x) * N_ks[j].diff(s.x) // J
                # AUX.display()
                # AUX.simplify()
                # AUX.display()

                # print('[%f, %f]'%(x_inf, x_sup))
                k[-1][i, j] += (AUX).integrate(s.x, x_inf, x_sup)
                # dNj = N_ks[i] * N_ks[j].diff(x)
                # k[-1][i, j] -= (dNj.at(x_sup) - dNj.at(x_inf))

            for i in range(m):
                # Vezes o jacobiano??
                f[-1][i] = (N_ks[i] * J).integrate(s.x, x_inf, x_sup)

            # displayEq('k_{%d}' % i_ks, k[-1])
            # displayEq('f_{%d}' % i_ks, f[-1])

        return k, f

    def construir_matriz_global(s, cp_x, knt_spans):
        # FIXME: calcular dimensoes das matrizes de forma decente
        m = s.p + 1
        M = len(cp_x)
        s.K = sp.zeros(M)
        s.F = sp.zeros(M, 1)

        k, f = s.matrizes_locais(m, s.knots, cp_x, knt_spans)

        # iel = tuple((x, x+1) for x in range(len(k)))

        for i, kfi in enumerate(zip(k, f)):
            ki, fi = kfi
            for l in range(m):
                for c in range(m):
                    s.K[l + i, c + i] += ki[l, c]
                s.F[l + i] += fi[l]
                #             print('F[%d] <- f%d[%d]'%(l+i, i, l))

        # # elementos aleijados
        #     s.m_global[ s.iel[ 0][ 0], s.iel[ 0][ 0] ] += sp.integrate(sp.diff(s.bases[1](x), x) ** 2, (x, 0, h)).subs({h:s.elementos[ 0].h})
        #     s.m_global[ s.iel[-1][-1], s.iel[-1][-1] ] += sp.integrate(sp.diff(s.bases[0](x), x) ** 2, (x, 0, h)).subs({h:s.elementos[-1].h})

        #     s.f_global[ s.iel[ 0][ 0] ] += sp.integrate(f(x                 ) * s.bases[1](x), (x, 0, h)).subs({h:s.elementos[ 0].h})
        #     s.f_global[ s.iel[-1][-1] ] += sp.integrate(f(x + s.elementos[-1].L0+s.elementos[-1].h) * s.bases[0](x), (x, 0, h)).subs({h:s.elementos[-1].h})

        # displayEq('K_global', s.K)
        # displayEq('F_global', s.F)

    def solve(s):
        ### [Usando gambiarra para aplicar condições de contorno]

        # alphas = sp.Matrix(sp.var('alpha0:%d' % (s.K.shape[0])))
        # sp.solve(s.K * alphas - s.F, alphas)

        s.u = sp.zeros(s.F.shape[0], s.F.shape[1])
        s.u[1:, 0] = s.K[1:, 1:].inv() * s.F[1:, 0]
        return s.u

    def plotSolucao(s, title=None):
        if s.cp_x is not None:
            P = [s.cp_x, s.u]
            nb.curvaSpline2D(s.N, P, title, cor='r')

    def plotDiffSolucao(s):
        if s.cp_x is not None:
            P = [s.cp_x, s.u]
            nb.curvaSpline2D([n.diff(s.x) for n in s.N], P, title='Derivada da solução')

    # def iga(s):
    #     s.construir_matriz_global()
    #     return s.solve()