# -*- coding: utf-8 -*-

__author__ = 'jhuancedro'
__email__ = 'jhuancedro@gmail.com'
__all__ = ['multiEq', 'displayEq', 'displaySub', 'display']

from sympy import var, Eq
from IPython.display import display
from functools import reduce


def multiEq(*Eqs):
    """Dado *Eqs = (a, b, ..., z) retorna:
        a = b = ... = z (sympy.Eq "aninhados")"""
    converte = lambda a: var(a + '\ ') if (type(a) is str) else a
    Eqs = map(converte, Eqs)
    return reduce(lambda x, y: Eq(x, y, evaluate=False), Eqs)


def displayEq(*Eqs):
    """Dado *Eqs = (a, b, ..., z) mostra (display):
        a = b = ... = z """
    display(multiEq(*Eqs))


def displaySub(A, B):
    """Mostra (displayEq) A[i] = B[i] e retorna conjunto substituições"""
    subs = list(zip(A, B))
    for a, b in subs:
        displayEq(a, b)
    return subs
