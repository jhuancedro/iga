
# coding: utf-8

# In[1]:


from itertools import product, cycle

import pylab as pl
import sympy as sp

from lib.AuxDisplay import *

sp.init_printing(use_latex=True)

#get_ipython().magic('matplotlib inline')


# In[2]:


x = sp.Symbol('x')




class FuncaoSegm:
    '''Define funcoes simbolicas para cada intervalo entre dois knots'''
    cycol = cycle('bgrcmk')
    def __init__(s, knots, i, p = None):
        s.i = i
        s.p = p
        s.knots = knots
        s.ke = sp.zeros(len(knots)-1, 1)
        
    def plot(s, a='N'):
        s.cor = next(FuncaoSegm.cycol)
        for k in range(len(knots)-1):
            X = pl.linspace(knots[k], knots[k+1], 50)
            pl.plot(X, [s.ke[k].subs({x:xi}) for xi in X], label=('%s %d,%d'%(a, s.i, s.p) if k==0 else None), c = s.cor)
            
    def op_e(s, op, num):
        '''Retorna a FuncaoSegm operada com escalar'''
        fs = FuncaoSegm(s.knots, s.i, s.p)
        for k in range(len(s.ke)):
            fs.ke[k] = op(s.ke[k], num)
        return fs
    
    def op_fs(s, op, f):
        '''Retorna a FuncaoSegm dividida por outra'''
        fs = FuncaoSegm(s.knots, s.i, s.p)
        for k in range(len(s.ke)):
            fs.ke[k] = op(s.ke[k], f.ke[k])
        return fs
    
    def _auxOp_(s, op, b):
        if type(b) is FuncaoSegm:  return s.op_fs(op, b)
        else:                      return s.op_e(op, b)
    
    def      __add__(s, b): return s._auxOp_(lambda x, y: x+y, b)  # +
    def      __mul__(s, b): return s._auxOp_(lambda x, y: x*y, b)  # *
    def  __truediv__(s, b): return s._auxOp_(lambda x, y: x/y, b)  # /
    def __floordiv__(s, b): return s._auxOp_(lambda x, y: x/y if y!=0 else 0, b)  # ///
    def     __iadd__(s, b): return s.__add__(b)  # +=
    def     __radd__(s, b): return s + b
    def expand(s): s.ke = s.ke.expand()
    
    def at(s, x0):
        '''Retorna o valor da FuncaoSegm em x0'''
        if x0 < s.knots[0] or x0 > s.knots[-1]:
            return 0
        for k in range(len(s.knots)-1):
            if x0 < s.knots[k+1]:
                return s.ke[k].subs({x: x0}).doit()
            
    def diff(s, var): return s.op_e(lambda x, y: sp.diff(x, y), var)
    
    def integrate(s, x, inf, sup):
        I = 0
        for k in range(len(s.ke)):
            if s.knots[k+1] > inf:
                a, b = s.knots[k:k+2]
                
                if s.knots[k] <= inf and inf < s.knots[k+1]: a = inf
                if s.knots[k] <= sup and sup < s.knots[k+1]: b = sup
                    
                # print('int[%f -> %f]'%(a, b))
                I += sp.integrate(s.ke[k], (x, a, b))
    
                if b == sup or b == s.knots[-1]: break
        
        return I
            
    def display(s, a='N'):
        displayEq('%s_{%d%d}'%(a, s.i, s.p), sp.Matrix(s.ke))


# In[4]:


def funcoesBases(p, knots, plotar = False):
    '''Cria a n funcoes bases B-Spline de grau p para um vetor de knots
        N[grau][i-esima funcao][entre knots k e k+1]
    '''
    nk = len(knots)
    N = [ [ FuncaoSegm(knots, i, pi) for i in range(nk-1-pi)] for pi in range(p+1)]
    cycol = cycle('bgrcmk')
    
    # FIXME: o parametro recebido eh p, porem o primeiro for itera sobre uma variavel p
    for p, Np in enumerate(N):
        if plotar: pl.figure()
            
        for i in range(len(Np)):
            if p == 0:
                Np[i].ke[i] = 1
            else:
                if knots[i+p] != knots[i]:
                    Np[i] += N[p-1][i] * ( (x-knots[i])/(knots[i+p]-knots[i]) ) 
                if knots[i+p+1] != knots[i+1]:
                    Np[i] += N[p-1][i+1] * ( (knots[i+p+1]-x)/(knots[i+p+1]-knots[i+1]) ) 
                    
                Np[i].expand()
            if plotar: Np[i].plot()
            # Npi.display()
                
        if plotar: plotShow('Funções de grau %d'%p)
            
    return N


# In[23]:


def curvaSpline2D(pontos, knots, bases):
    '''Plota os vertices de controle e a curva dados os 
        pontos de controle, vetor de knots e funcoes bases'''
    Px, Py = pontos
    pl.plot(Px, Py, 'k--', label='Vertices de controle') # plotar vertices
    pl.plot(Px, Py, 'ro', label='Pontos de controle')  # plotar pontos
    
    nk = len(knots)
    Cx = FuncaoSegm(knots, bases[0].i, p)
    Cy = FuncaoSegm(knots, bases[0].i, p)
    for i, Ni in enumerate(bases):
        Cx = Cx + ( Ni * (Px[i]) )
        Cy = Cy + ( Ni * (Py[i]) )

    cor = next(FuncaoSegm.cycol)
    for k in range(nk-1):
        X = pl.linspace(knots[k], knots[k+1], 50)
        pl.plot([Cx.ke[k].subs({x:xi}) for xi in X],
                [Cy.ke[k].subs({x:xi}) for xi in X], c=cor)

    plotShow()


# In[5]:


def plotShow(title = None):
    if title is not None: pl.title(title)
    pl.grid()
    pl.legend(loc='best')
    pl.show() 


# In[6]:


def plotFuncoesSeg(N, a='N', title = None): 
    for Ni in N: Ni.plot(a)
    plotShow(title)


# In[7]:


def clamped_uniform_knots(inicio, fim, n, p):
    return [inicio]*p + list(pl.linspace(inicio, fim, n-p+1)) + [fim]*p


# ### Exemplo 1

# In[29]:


p = 2

knots = [0, 0, 0, 0.5, 1, 1, 1]
displayEq('\Xi', sp.Array(knots))

N = funcoesBases(p, knots, plotar=False)[-1]

for Ni in N:
    Ni.plot('N\'')
plotShow()

for Ni in N:
    Ni.diff(x).plot('N\'')
plotShow(title='Derivadas de das funçoes de grau %d'%p)

for Ni in N:
    X = pl.linspace(0, 1, 100)
    Ni_ = Ni.diff(x)
    pl.plot(X, [Ni_.integrate(x, 0, xi) for xi in X], label='N')
plotShow(title='Integrais das derivadas de das funçoes de grau %d'%p)


# ## Montando matrizes locais e globais

# In[30]:


get_ipython().magic('whos')


# In[18]:


n = 4
p = 2
x = sp.Symbol('x')

knots = (0, 0, 0, 0.5, 1, 1, 1)
displayEq('\Xi', sp.Array(knots))

# Pontos de controle em x
cp_x = (0.0, 0.5, 1.5, 2.0)

knt_spans = [(0, 3), (3, 6)]

N = funcoesBases(p, knots, plotar=True)[-1]


# In[19]:


A, E, rho, g = sp.var('A, E, rho, g')
def matrizes_locais(m, knots, cp_x, knt_spans):
    k = []
    f = []
    for ki, ks in enumerate(knt_spans):
#         print('ks:', ks)
        x_inf, x_sup = knots[ks[0]], knots[ks[1]]
        cp_x_ks = cp_x[ki: ki+p+1]
        N_ks    =    N[ki: ki+p+1]
        
#         for i, n in enumerate(N_ks):
#             n.plot('N %d'%i)
#         pl.xlim(x_inf, x_sup)
#         plotShow()
        print('cp_x_ks:', cp_x_ks)
#         print('[%f, %f]'%(x_inf, x_sup))

        J = sum(N_ks[i].diff(x) * cp_x_ks[i] for i in range(m))
#         J.display('J')

        k.append( sp.zeros(3) )
        f.append(sp.zeros(3, 1))
        for i, j in product(range(m), repeat = 2):
            k[-1][i, j] += (N_ks[i].diff(x) * N_ks[j].diff(x) // J).integrate(x, x_inf, x_sup)
            # dNj = N_ks[i] * N_ks[j].diff(x)
            # k[-1][i, j] -= (dNj.at(x_sup) - dNj.at(x_inf))

        for i in range(m):
            # Vezes o jacobiano??
            f[-1][i] = (N_ks[i] * J).integrate(x, x_inf, x_sup)
            
        displayEq('k_{%d}'%ki, k[-1])
        displayEq('f_{%d}'%ki, f[-1])
        
    return k, f


# In[20]:


def construir_matriz_global():
    m = p + 1
    M = m + 1
    K = sp.zeros(M)
    F = sp.zeros(M, 1)

    k, f = matrizes_locais(m, knots, cp_x, knt_spans)

    # iel = tuple((x, x+1) for x in range(len(k)))

    for i, kfi in enumerate(zip(k, f)):
        ki, fi = kfi
        for l in range(m):
            for c in range(m):
                K[l+i, c+i] += ki[l, c]
            F[l+i] += fi[l]
#             print('F[%d] <- f%d[%d]'%(l+i, i, l))

    #     # elementos aleijados
    #     s.m_global[ s.iel[ 0][ 0], s.iel[ 0][ 0] ] += sp.integrate(sp.diff(s.bases[1](x), x) ** 2, (x, 0, h)).subs({h:s.elementos[ 0].h})
    #     s.m_global[ s.iel[-1][-1], s.iel[-1][-1] ] += sp.integrate(sp.diff(s.bases[0](x), x) ** 2, (x, 0, h)).subs({h:s.elementos[-1].h})

    #     s.f_global[ s.iel[ 0][ 0] ] += sp.integrate(f(x                 ) * s.bases[1](x), (x, 0, h)).subs({h:s.elementos[ 0].h})
    #     s.f_global[ s.iel[-1][-1] ] += sp.integrate(f(x + s.elementos[-1].L0+s.elementos[-1].h) * s.bases[0](x), (x, 0, h)).subs({h:s.elementos[-1].h})

    displayEq('K_global', K)
    displayEq('F_global', F)
    
    return K, F


# In[21]:


# k, f = matrizes_locais(m, knots, cp_x, knt_spans)
K, F = construir_matriz_global()


# In[13]:


alphas = sp.Matrix(sp.var('alpha0:%d'%(K.shape[0])))
sp.solve(K*alphas-F, alphas)


# In[15]:


u = sp.Matrix([0.0, 1.0, 2.0, 2.0])
displayEq('u', u)


# In[26]:


P = [cp_x, u]
curvaSpline2D(P, knots, bases=N)


# In[17]:


cp_x


# In[ ]:




