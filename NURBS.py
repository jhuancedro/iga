import sympy as sp
import pylab as pl
from AuxDisplay import *
from itertools import cycle
from scipy.integrate import quad

# sp.init_printing(use_latex=True)
# %matplotlib inline

class FuncaoSegm:
    """Define funcoes simbolicas para cada intervalo entre dois knots"""
    cycol = cycle('bgrcmyk')

    # cycol = cycle({'b', 'g', 'r', 'c', 'm', 'y', 'k', 'w'})
    # cycol = cycle({'C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'})

    def __init__(s, knots, p=None, x='x'):
        s.p = p if p is not None else 0
        s.knots = knots
        s.ke = sp.zeros(len(knots) - 1, 1)
        s.cor = next(FuncaoSegm.cycol)
        s.x = sp.Symbol(x)

    def plot(s, a='N', lim=None):
        if lim is None:
            lim = [s.knots[0], s.knots[-1]]
        for k in range(len(s.knots) - 1):
            X = [xi for xi in pl.linspace(s.knots[k], s.knots[k + 1], 50) if (lim[0] <= xi <= lim[1])]
            pl.plot(X, [s.ke[k].subs({s.x: xi}) for xi in X],
                    c=s.cor)
                    # label=('%s p=%d' % (a, s.p) if k == 0 else None), c=s.cor)

    def op_e(s, op, num):
        """Retorna a FuncaoSegm operada com escalar"""
        fs = FuncaoSegm(s.knots, s.p)
        for k in range(len(s.ke)):
            fs.ke[k] = op(s.ke[k], num)
        return fs

    def op_fs(s, op, f):
        """Retorna a FuncaoSegm dividida por outra"""
        fs = FuncaoSegm(s.knots, s.p)
        for k in range(len(s.ke)):
            fs.ke[k] = op(s.ke[k], f.ke[k])
        return fs

    def _auxOp_(s, op, b):
        if type(b) is FuncaoSegm:
            return s.op_fs(op, b)
        else:
            return s.op_e(op, b)

    def __add__(s, b):
        return s._auxOp_(lambda x, y: x + y, b)  # +

    def __mul__(s, b):
        return s._auxOp_(lambda x, y: x * y, b)  # *

    def __truediv__(s, b):
        return s._auxOp_(lambda x, y: x / y, b)  # /

    def __floordiv__(s, b):
        return s._auxOp_(lambda x, y: x / y if y != 0 else 0, b)  # ///

    def __iadd__(s, b):
        return s.__add__(b)  # +=

    def __radd__(s, b):
        return s + b

    def expand(s):
        s.ke = s.ke.expand()

    def simplify(s):
        for k, _ in enumerate(s.ke):
            s.ke[k] = s.ke[k].simplify()

    def doit(s):
        s.ke = s.ke.doit()

    def at(s, x0):
        """Retorna o valor da FuncaoSegm em x0"""
        if x0 < s.knots[0] or x0 > s.knots[-1]:
            return 0
        for k in range(len(s.knots) - 1):
            if x0 < s.knots[k + 1]:
                return s.ke[k].subs({s.x: x0}).doit()

    def diff(s, var):
        """Retorna a primeira derivada da FuncaoSegm em relacao a var"""
        return s.op_e(lambda x, y: sp.diff(x, y), var)

    @staticmethod
    def symb_integrate(f, args):
        """Integra simbolicamente uma função simbólica"""
        return sp.integrate(f.ratsimp(), args)

    @staticmethod
    def num_integrate(f, args):
        """Integra numericamente uma função simbólica"""
        x, inf, sup = args
        return quad(lambda x_: f.subs({x: x_}), inf, sup)[0]

    def integrate(s, x, inf, sup):
        I = 0
        for k in range(len(s.ke)):
            if s.knots[k + 1] > inf:
                a, b = s.knots[k:k + 2]

                if s.knots[k] <= inf < s.knots[k + 1]: a = inf
                if s.knots[k] <= sup < s.knots[k + 1]: b = sup

                # print('int[%f -> %f]' % (a, b))
                # I += FuncaoSegm.symb_integrate(s.ke[k], (x, a, b))
                I += FuncaoSegm.num_integrate(s.ke[k], (x, a, b))

                if b == sup or b == s.knots[-1]: break

        return I

    def display(s, a='N'):
        displayEq('%s_{%d}' % (a, s.p), sp.Matrix(s.ke))


def plotShow(title=None, nFig=1):
    if title is not None: pl.title(title)
    for i in range(nFig):
        pl.figure(i+1)
        pl.grid()
        pl.legend(loc='best')
    pl.show()


def plotFuncoesSeg(N, a='N', title=None):
    """Plota uma lista de FuncaoSegm"""
    # for i, Ni in enumerate(N): Ni.plot(a + ' %d' % i)
    for i, Ni in enumerate(N): Ni.plot()
    plotShow(title)


def clamped_uniform_knots(inicio, fim, n, p):
    return [inicio] * p + list(pl.linspace(inicio, fim, n - p + 1)) + [fim] * p


def curvaSpline2D(base, pontos, title=None, cor=None):
    """Plota os vertices de controle e a curva dados os
        pontos de controle e funcoes bases"""

    Px, Py = pontos
    pl.figure(1)
    pl.plot(Px, Py, 'k--', label='Vertices de controle')  # plotar vertices
    pl.plot(Px, Py, 'ro', label='Pontos de controle')  # plotar pontos

    knots = base[0].knots
    nk = len(knots)

    Cx = FuncaoSegm(knots, base[0].p)
    Cy = FuncaoSegm(knots, base[0].p)
    for i, Ni in enumerate(base):
        Cx = Cx + (Ni * (Px[i]))
        Cy = Cy + (Ni * (Py[i]))
    # Cx.display('Cx')
    # Cy.display('Cy')
    #
    # Cx.plot('Cx%d'%i)
    # Cy.plot('Cy%d'%i)


    if cor is None: cor = next(FuncaoSegm.cycol)
    eixo_x, eixo_y = [], []
    # for k in range(nk - 1):
    for k in range(1, nk - 2):
        X = pl.linspace(knots[k], knots[k + 1], 100 )
        aux_x = [Cx.ke[k].subs({Cx.x: xi}) for xi in X]
        aux_y = [Cy.ke[k].subs({Cy.x: xi}) for xi in X]
        pl.figure(1)# 'Solução'
        pl.plot(aux_x[::5], aux_y[::5], 'rx')#c=cor)
        eixo_x.extend( aux_x )
        eixo_y.extend( aux_y )

        dy_dx = []
        for i in range(len(aux_x)-1):
            dx = aux_x[i+1] - aux_x[i]
            dy_dx.append( (aux_y[i+1] - aux_y[i]) / dx if dx != 0 else 0 )
        pl.figure(2) # 'Derivada'
        # pl.plot((aux_x[0], aux_x[-2]), (dy_dx[0], dy_dx[-1]), 'ro', label='Derivada da solução' if k==1 else '')
        pl.plot(aux_x[:-1:5], dy_dx[::5], 'rx', label='Derivada da solução' if k==1 else '')
    # plotShow(title)

    # pl.figure(1)
    # pl.grid()
    # pl.legend(loc='best')
    # pl.show()
    #
    # pl.figure(2)
    # pl.grid()
    # pl.legend(loc='best')
    # pl.show()


    plotShow(title, nFig=2)


# class FSArray:
#     """Define array de funçções segmentadas"""
#     def __init__(s):
#         s.


class BasesNURBS:
    """Estrutura com n funcoes bases B-Spline de grau p para um vetor de knots
        N[grau][i-esima funcao][entre knots k e k+1]
    """

    def __init__(s, knots, p=0, x='x'):
        s.p = 0
        s.knots = knots
        s.x = sp.Symbol(x)
        s.nk = len(knots)
        s.N = None
        s.base0()
        s.elevarGrau(p)

    def base0(s):
        """Define N[0] com funcoes bases de grau 0"""
        s.N = [[]]
        for i in range(s.nk - 1):
            s.N[0].append(FuncaoSegm(s.knots, 0))
            s.N[0][-1].ke[i] = 1

    def elevarGrau(s, p=None):
        """Adiciona aas bases N uma base de grau p+1"""
        if p is None:
            p = s.p + 1
        elif p <= s.p:
            return False
        elif p - s.p > 1:
            s.elevarGrau(p - 1)

        s.N.append([])  # adiciona lista de funcoes de grau p+1
        for i in range(len(s.N[-2]) - 1):
            p = s.p + 1
            Npi = FuncaoSegm(s.knots, p)
            if s.knots[i + p] != s.knots[i]:
                Npi += s.N[p - 1][i] * ((s.x - s.knots[i]) / (s.knots[i + p] - s.knots[i]))
            if s.knots[i + p + 1] != s.knots[i + 1]:
                Npi += s.N[p - 1][i + 1] * ((s.knots[i + p + 1] - s.x) / (s.knots[i + p + 1] - s.knots[i + 1]))

            Npi.expand()
            # Npi.display()
            s.N[p].append(Npi)
        s.p += 1

    def decairGrau(s, p=None):
        """Remove das bases N a base de grau p"""
        if p is None:
            p = s.p - 1
        elif p >= s.p:
            return False
        elif p - s.p < 1:
            s.decairGrau(p + 1)

        s.N.pop()
        s.p -= 1

    @staticmethod
    def knt_h_refin(knt, p):
        """Retorna uma lista refinada de knots"""
        n_knt = list(knt)
        for k in range(len(knt) - p - 2, p - 1, -1):
            n_knt.insert(k + 1, (n_knt[k] + n_knt[k + 1]) / 2.0)
        # print(s.p, n_knt)

        return n_knt

    def refinamento_h(s):
        """Retorna uma estrutura BasesNURBS com o knots refinado"""
        return BasesNURBS(BasesNURBS.knt_h_refin(s.knots, s.p), 2)

    def plotBase(s, p=None):
        p = s.p if p is None else p
        plotFuncoesSeg(s.N[p], title='Funções de grau %d' % p)

    def curvaSpline2D(s, pontos, p=None, title=None):
        """Plota os vertices de controle e a curva dados os
            pontos de controle e grau p das funcoes bases"""

        if p is None:
            p = s.p
        elif p > s.p:
            return False

        curvaSpline2D(s.N[p], pontos, title)

    def basePonderada(s, W, p=None, plotar=False):
        """Retorna a base de grau p ponderada com os pesos W"""
        if p is None:
            p = s.p
        elif p > s.p:
            return False
        base = s.N[p]

        sum_Nw = FuncaoSegm(s.knots, p)
        for Ni, wi in zip(base, W):
            sum_Nw += Ni * wi
        n = len(W)
        R = [0] * n
        for i, Ni, wi in zip(range(n), base, W):
            R[i] = Ni * wi // sum_Nw

        if plotar: plotFuncoesSeg(R, a='R', title='Funções de grau %d com pesos' % p)

        return R

    def diff(s, var, p=None):
        p = s.p if p is None else p
        dB = [0] * len(s.N[p])
        for i, Npi in enumerate(s.N[p]):
            dB[i] = Npi.diff(var)

        return dB
